class HumanPlayer
  def initialize(name)
    @name = name
  end

  attr_reader :name

  def get_play
    puts "Pick a point to strike:"
    coordinates = gets.chomp
    x = coordinates[0].to_i
    y = coordinates[-1].to_i
    [x, y]
  end
end
