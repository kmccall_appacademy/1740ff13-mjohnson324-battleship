class Board
  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  attr_accessor :grid

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def []=(position_x, position_y, strike)
    @grid[position_x][position_y] = strike
  end

  def [](position)
    position_x, position_y = position
    @grid[position_x][position_y]
  end

  def count
    count = 0
    @grid.each do |row|
      row.each { |ship| count += 1 if ship }
    end
    count
  end

  def empty?(position = @grid)
    if position != @grid
      row, column = position
      @grid[row][column].nil?
    else
      ship_detection = position.flatten.compact
      ship_detection.empty?
    end
  end

  def full?
    ship_detection = @grid.flatten
    return true if ship_detection.compact == ship_detection
    false
  end

  def place_random_ship
    raise "The board is full" if full?
    last_index = @grid.length - 1
    randomize = (0..last_index).to_a

    ship_placed = false
    until ship_placed
      row = randomize.shuffle[0]
      column = randomize.shuffle[0]
      if @grid[row][column] == nil
        @grid[row][column] = :s
        ship_placed = true
      end
    end
  end

  def won?
    return true if empty?
    false
  end
end
