class BattleshipGame
  def initialize(player, board = Board.new)
    @player = player
    @board = board
  end

  attr_reader :board, :player

  def play_turn
    attack_point = @player.get_play
    attack(attack_point)
  end

  def game_over?
    @board.won?
  end

  def count
    @board.count
  end

  def attack(position)
    row = position[0]
    column = position[1]
    @board[row, column] = :x
  end
end
